package com.example.firstminiproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DynamicView {

    Context ct;


    public DynamicView() {
    }

    public DynamicView(Context context) {
        this.ct = context;
    }

    @SuppressLint("ResourceAsColor")
    public FrameLayout frame(Context context, String title, String description){
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(900,900);
        TextView textView1,textView2;
        textView1 = Title(context,title);
        textView2 = Desc(context,description);
        FrameLayout layout = new FrameLayout(context);
        layout.setLayoutParams(params);
        layout.setBackgroundColor(R.color.colorPrimary);
        layout.addView(textView1);
        layout.addView(textView2);
        return layout;
    }


    @SuppressLint("ResourceAsColor")
    public TextView Title(Context context , String title){
        final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(18);
        textView.setTextColor(R.color.colorWrite);
        textView.setText(title);
        textView.setPadding(20,20,20,20);
        return textView;
    }

    @SuppressLint("ResourceAsColor")
    public TextView Desc(Context context , String description){
        final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(12);
        textView.setTextColor(R.color.colorWrite);
        textView.setText(description);
        return textView;
    }

}
