package com.example.firstminiproject;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;


import androidx.annotation.Nullable;


public class RoundReactCornerImageView extends androidx.appcompat.widget.AppCompatImageView {

    private float radius = (float) 20.0;
    private Path path;
    private RectF rect;

    public RoundReactCornerImageView(Context context) {
        super(context);
        init();
    }

    public RoundReactCornerImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RoundReactCornerImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        path = new Path();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        rect = new RectF(0, 0, this.getWidth(), this.getHeight());
        path.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(path);
        super.onDraw(canvas);
    }
}
