package com.example.firstminiproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Intent intent = new Intent();

    FrameLayout flGallery,flContact,flCallPhone,flProfile,flNoteBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        flGallery = findViewById(R.id.gallery);
        flContact = findViewById(R.id.contact);
        flCallPhone = findViewById(R.id.call);
        flProfile = findViewById(R.id.your_profile);
        flNoteBook = findViewById(R.id.notebook);

        flGallery.setOnClickListener(this);
        flContact.setOnClickListener(this);
        flCallPhone.setOnClickListener(this);
        flProfile.setOnClickListener(this);
        flNoteBook.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.gallery:
                intent.setClass(MainActivity.this,GalleryScreen.class);
                startActivity(intent);
                break;
            case R.id.contact:
                intent.setClass(MainActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
            case R.id.call:
                intent.setClass(MainActivity.this,CallActivity.class);
                startActivity(intent);
                break;
            case R.id.your_profile:
                intent.setClass(MainActivity.this,ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.notebook:
                intent.setClass(MainActivity.this,NoteScreen.class);
                startActivity(intent);
                break;
            default:
                Toast.makeText(this,"On click me",Toast.LENGTH_SHORT).show();
                break;
        }

    }
}