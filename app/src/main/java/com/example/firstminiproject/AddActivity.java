package com.example.firstminiproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_cancel,btn_save;
    EditText et_title,et_contact;

    String Title,Contact;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        intent = getIntent();

        btn_cancel = findViewById(R.id.btn_cancel);
        btn_save = findViewById(R.id.btn_save);

        et_title = findViewById(R.id.title);
        et_contact = findViewById(R.id.contact);

        btn_save.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save:
                Title = et_title.getText().toString();
                Contact = et_contact.getText().toString();
                if(Title.isEmpty() || Contact.isEmpty()){
                    Toast.makeText(AddActivity.this,"No Contact",Toast.LENGTH_SHORT).show();
                }else{
                    intent.putExtra("title",Title);
                    intent.putExtra("contact",Contact);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.btn_cancel:
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            default:
                Toast.makeText(AddActivity.this,"No Contact",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}